(** Constrained plugboards. *)

module Map = Symbol.Map
module Set = Symbol.Set

(* We represent the board as a mapping associating each symbol to the set of its
 * possible images (actually, we store the complement of this set, since
 * Symbol.Set does not support removing of elements…). For performance reasons
 * we also store the cardinal of this set.
 * To implement backtracking, we maintain a history of ‘remove_assoc’ operations
 * done, as well as a counter incremented after each operation, which allows to
 * easily reference a previous state. History entries are tuples of the form
 * (x,y,set_x,set_y) giving which association has been removed (again, we also
 * store the old values of the sets because we cannot remove elements from a set
 * directly…).
 * *)
type t = {
  mapping : (int * Set.t) Map.t;
  mutable history : (Symbol.sym * Symbol.sym * Set.t * Set.t) list;
  mutable hist_count : int;
}

let top () =
  { mapping = Map.make (Symbol.nb_syms, Set.empty);
    history = [];
    hist_count = 0 }

let possible {mapping;_} x y = not @@ Set.member x @@ snd @@ Map.get mapping y

let possibles constraints x =
  Symbol.fold (fun li y -> if possible constraints x y then y::li else li) []

(* We print a constrained board by printing the list of allowed associations. *)
let print file constraints =
  Symbol.iter (fun x ->
    Symbol.iter (fun y ->
      if x <= y && possible constraints x y then
        Printf.fprintf file "%c%c," (Symbol.to_char x) (Symbol.to_char y)
    )
  )

exception Impossible

(* Return the first symbol satisfying a predicate. It must exist. *)
let find_symbol predicate =
  let rec find x = if predicate x then x else find (Symbol.next x) in
  find Symbol.a

let remove_assoc constraints =
  (* Forbid the association between x and y. *)
  let rec remove_assoc x y =
    if possible constraints x y then begin
      let (count_x,set_x) = Map.get constraints.mapping x
      and (count_y,set_y) = Map.get constraints.mapping y in
      (* signal if this removal would lead to an inconsistency: *)
      if count_x = 1 || count_y = 1 then
        raise Impossible;
      (* do the removal and log it: *)
      constraints.history <- (x,y,set_x,set_y) :: constraints.history;
      constraints.hist_count <- constraints.hist_count + 1;
      Map.set constraints.mapping x (count_x-1, Set.add y set_x);
      Map.set constraints.mapping y (count_y-1, Set.add x set_y);
      (* force associating symbols if they have no other alternative: *)
      if count_x = 2 then
        force_assoc x;
      if count_y = 2 then
        force_assoc y
    end
  (* Force the only possible association of x that remains (this is not an
   * elementary operation as it actually uses ‘remove_assoc’, thus for the
   * history we only have to consider remove operations). *)
  and force_assoc x =
    let y = find_symbol (possible constraints x) in
    Symbol.iter (fun z -> if z <> x then remove_assoc y z)
  in
  remove_assoc

(* A state is simply a pointer to the board and the history counter at the point
 * we want to restore. *)
type state = t * int (* [David] parfait *)

let save constraints = (constraints, constraints.hist_count)

let restore (constraints, hist_count) =
  assert (hist_count <= constraints.hist_count);
  (* undo every operation done since the point given by ‘hist_count’: *)
  while hist_count < constraints.hist_count do
    let (x,y,set_x,set_y) = List.hd constraints.history in
    constraints.history <- List.tl constraints.history;
    constraints.hist_count <- constraints.hist_count - 1;
    let (count_x,_) = Map.get constraints.mapping x
    and (count_y,_) = Map.get constraints.mapping y in
    Map.set constraints.mapping x (count_x+1, set_x);
    Map.set constraints.mapping y (count_y+1, set_y)
  done
