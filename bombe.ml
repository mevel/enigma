(** Implementation of a Enigma bombe: given a cleartext and its ciphertext,
 * print the possible configurations of an Enigma machine. *)

(**/**)
module Map = Symbol.Map

let id = Map.init (fun x -> x)

let (%%) m1 m2 = Map.map (Map.get m2) m1    (* map composition *)
let (>=) x m = Map.get m x
and (>+) x r = Rotor.image r x
and (>-) x r = Rotor.pre_image r x

(* Compute the image of a symbol through the Enigma machin with configaration
 * given, assuming the plugboard is the identity. *)
let run rot1 rot2 rot3 refl =
  let i0 = Symbol.to_int rot1.Rotor.pos
  and j0 = Symbol.to_int rot2.Rotor.pos in
  fun off ->
    let i1 = off mod Symbol.nb_syms
    and j1 = off/Symbol.nb_syms mod Symbol.nb_syms
    and k1 = off/Symbol.nb_syms/Symbol.nb_syms mod Symbol.nb_syms in
    let i = i1 in
    let j = j1 + (i0+i)/Symbol.nb_syms in
    let k = k1 + (j0+j)/Symbol.nb_syms in
    let rot1 = Rotor.steps rot1 i
    and rot2 = Rotor.steps rot2 j
    and rot3 = Rotor.steps rot3 k in
    fun x -> x >+ rot1 >+ rot2 >+ rot3 >= refl >- rot3 >- rot2 >- rot1
(**/**)

(** Given the rotors and reflectors, compute constraints on the plugboard by
  * analyzing cycles in a clear-cipher graph. *)
let infer_plug_constraint rot1 rot2 rot3 refl =
  (* Create a mapping representing the machine at offset [off]. *)
  (* [David] Bonne idée de précalculer mais c'est bizarre de s'arrêter
   * là: pourquoi ne pas éviter de recalculer la machine deux fois
   * au même offset ? Après lecture de ton code, je vois que tu
   * évites une grosse partie de cette redondance. *)
  let machine off = Map.init (run rot1 rot2 rot3 refl off) in
  (* Deduce constraints from a particular cycle. *)
  let treat_cycle constraints =
    (* For each vertice xi of the cycle, p(xi) is a fixpoint of the mapping
     * obtained by chaining the machines along the cycle starting from xi. Thus,
     * constraints consist in removing associations between xi and symbols other
     * than such a fixpoint.
     * We can avoid recomputing the machine chain for each vertice: just compute
     * it once for the first vertice x0; then, the chain for the next vertice x1
     * can be deduced by composing on both sides (because machines are
     * involutive) the previous chain with the machine associated with the edge
     * (x0-x1). [David] malin!!! *)
    let treat_each_vertice =
      List.fold_left
        (fun chain (x,m) ->
           Symbol.iter (fun y ->
             if y <> Map.get chain y then
               Board.remove_assoc constraints x y
           );
           m %% chain %% m
        )
    in
    fun cycle ->
      (* We precompute the machine associated with each edge. *)
      let elts = List.map (fun (x,i) -> (x, machine i)) cycle in
      let chain0 = List.fold_left (fun chain (_,m) -> chain %% m) id elts in
      treat_each_vertice chain0 elts
      |> ignore
  in
  (* Treat each cycle in turn. *)
  fun cycles ->
    let constraints = Board.top () in
    Cycles.iter_on_cycles (treat_cycle constraints) cycles;
    constraints

(** {2 The [bomb] program} *)

(** Iterate on each rotor position. *)
let mpos rot f =
  Symbol.iter (fun x ->
    f {rot with Rotor.pos = x}
  )

(** Iterate on each choice of an element in the list; the itered function also
  * receives the list of remaining elements. *)
let melem li (f : _ -> unit) =
  let rec iter left = function
    | []       -> ()
    | e::right -> f (e, List.rev_append left right); iter (e::left) right
  in
  iter [] li

(** The ‘bombe’ program: [bombe f] reads a cleartext and a ciphertext from the
  * command line, then tries every Enigma machine (rotors drawn from
  * [Rotors.rotors] with arbitrary positions). For each configuration which do
  * not appear to be impossible according to [infer_plug_constraint], it passes
  * it to [f]. *)
let bombe f =
  match List.tl @@ Array.to_list Sys.argv with
  | clear :: cipher :: [] ->
      let clear  = String.uppercase clear
      and cipher = String.uppercase cipher in
      let g = Cycles.graph_of_known_cipher clear cipher in
      let cycles = Cycles.cycles g in
      let refl = Involution.reflector in
      (* [David] bonne utilisation de melem/mpos, enfin! *)
      melem Rotor.rotors (fun (rot1,other_rotors) ->
      melem other_rotors (fun (rot2,other_rotors) ->
      melem other_rotors (fun (rot3,_) ->
      mpos rot1 (fun rot1' ->
      mpos rot2 (fun rot2' ->
      mpos rot3 (fun rot3' ->
      try
        let constraints = infer_plug_constraint rot1' rot2' rot3' refl cycles in
        f rot1' rot2' rot3' refl constraints clear cipher
      with Board.Impossible -> ()
      ))))))
  | _ ->
      Printf.fprintf stderr "usage:\n%s CLEAR CIPHER\n" Sys.argv.(0);
      exit 1

(** {2 The [brute] program} *)

(** Force a plugboard association. Raise [Board.Impossible] if this is not
  * possible. *)
let force_assoc constraints x y =
  Symbol.iter (fun z ->
    if z <> y then
      Board.remove_assoc constraints x z;
    if z <> x then
      Board.remove_assoc constraints y z
  )

(** [mforce_assoc p x f] tries to force the association between [x] and [y] in
  * [p] and, if it succeeded, call [f] with the plugboard modified. Any
  * [Board.Impossible] exception it might raise will be caught. In any case, the
  * state of the board is restored at the end. *)
let mforce_assoc constraints x y f =
  if Board.possible constraints x y then begin
    let state = Board.save constraints in
    begin try
      force_assoc constraints x y;
      f ()
    with Board.Impossible -> ()
    end;
    Board.restore state
  end

(** [mget p x f] iterate [f] on values that can be associated to [x]. *)
let mget constraints x f =
  Symbol.iter (fun y ->
    mforce_assoc constraints x y (fun () -> f y)
  )

(*
(** The ‘brute’ program: given the result of a bombe, compute more constraints
 * based on the cleartext and the ciphertext, and print the resulting
 * plugboards. *)
let brute rot1 rot2 rot3 refl constraints clear cipher =
  let compute = run rot1 rot2 rot3 refl in
  (* Check than the image of x is py as expected, and force the association. *)
  let test off px py =
    let y = compute off px in
    if not @@ Board.possible constraints y py then
      raise Board.Impossible;
    force_assoc constraints y py
  in
  let n = String.length clear in
  (* This function is called recursively to try each possible association for
   * each symbol, starting from symbol A. When it has successfully processed the
   * last symbol, we get a solution. *)
  let rec explore x =
    mget constraints x (fun px ->
      (* [David] je ne comprends pas, ce mforce_assoc est déja forcé *)
      mforce_assoc constraints x px (fun () ->
        (* check that our new association is consistent with the texts: *)
        (* [David] Ce n'est pas efficace: il est possible que tu énumères
         * beaucoup de x->px sans que le clear/cipher impose quoi que ce
         * soit. Il est plus avantageux de chercher les valeurs possibles
         * seulement pour les caractères pour lesquels clear/cipher va
         * contraindre l'espace de recherche. *)
        for off = 0 to n-1 do
          if clear.[off] = Symbol.to_char x then
            test off px (Symbol.of_char cipher.[off])
          else if cipher.[off] = Symbol.to_char x then
            test off px (Symbol.of_char clear.[off])
        done;
        (* if so, continue exploring, or print the solution if that was the last
         * symbol: *)
        let x' = Symbol.next x in
        if x' <> Symbol.a then
          explore x'
        else
          Printf.printf "possible plugboard:\n    %a\n%!" Board.print constraints
      )
    )
  in
  explore Symbol.a
*)

(** The ‘brute’ program: given the result of a bombe, [brute f] compute more
 * constraints based on the cleartext and the ciphertext, and pass
 * configurations found to [f]. *)
let brute f rot1 rot2 rot3 refl constraints clear cipher =
  let compute = run rot1 rot2 rot3 refl in
  let n = String.length clear in
  (* This function is called recursively to try each possible association for
   * each symbol in the cleartext. When it has successfully processed the whole
   * text, we get a solution. *)
  let rec explore off =
    if off = n then
      f rot1 rot2 rot3 refl constraints clear cipher
    else begin
      let x  = Symbol.of_char clear.[off]
      and py = Symbol.of_char cipher.[off] in
      mget constraints x (fun px ->
        (* check that our new association is consistent with the texts: *)
        let y = compute off px in
        if not @@ Board.possible constraints y py then
          raise Board.Impossible;
        force_assoc constraints y py;
        (* if so, continue exploring: *)
        explore (off+1)
      )
    end
  in
  explore 0

let output label rot1 rot2 rot3 refl constraints clear cipher =
  Printf.printf "[%s] rotors: %s-%s-%s\n"
    label (Rotor.to_string rot1) (Rotor.to_string rot2) (Rotor.to_string rot3);
  Printf.printf "[%s] assocs: %a\n%!"
    label Board.print constraints

(** Behaviour of the "bombe/brute" executables. *)
let () =
  match Filename.basename Sys.argv.(0) with
  | "bombe" ->
      bombe (output "bombe")
  | "brute" ->
      bombe (brute (output "brute"))
  | _ -> ()
