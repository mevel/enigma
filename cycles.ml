(** Algorithm computing the set of all (elementary) cycles of a graph. *)

type multicycle = (Symbol.sym * Graph.Positions.t) list
type cycle      = (Symbol.sym * int              ) list
type cycles = multicycle list

(* Convert a list of nodes into a [multicycle] by adding to the list the label
 * of the edges. *)
let multicycle_of_node_list g nodes =
  let first = List.hd nodes in
  let rec fold_edges = function
    | []          -> []
    | last :: []  -> (last, Graph.get_edge g last first) :: []
    | x :: y :: q -> (x, Graph.get_edge g x y) :: fold_edges (y::q)
  in
  fold_edges nodes

(* Determine if the cycle (given as a [Path.t] object) is useful, that is if it
 * has length at least three, and also if its second node is lesser than its
 * last node (this is to select only one instance among the two possible for
 * eack cycle, one for each orientation).
 * If so, return the cycle as a list of nodes. *)
let select_useful_cycle first path = (* [David] parfait *)
  let nodes = Path.rev_path path in
  let last = List.hd nodes
  and path_suffix = List.tl (List.rev nodes) in
  match path_suffix with
  | second :: _ when second <= last ->
      Some (first :: path_suffix)
  | _ ->
      None

let cycles g =
  (* Explore the paths starting at vertice [s]; add the cycles found to the
   * list [li]. *)
  let explore_from li0 s =
    let rec explore path =
      (* [David] enfin quelqun qui utilise naturellement mon fold ;)
       * Par contre tu aimes bien impliciter un maximum d'arguments
       * (par eta-contraction) je trouve ça souvent moins lisible.
       * Note que ça peut aussi être moins efficace -- ça a été le
       * cas avec certaines versions du compilo encore récemment. *)
      Graph.fold_over_connected g (fun li x ->
        (* if we reached the source back, consider adding the cycle: *)
        if x = s then
          match select_useful_cycle s path with
          | Some path -> path :: li
          | None      -> li
        (* else, if the vertice was already seen or if the source is not minimal
         * in the resulting path, drop the path: *)
        else if x <= s || Path.mem path x then
          li
        (* otherwise, continue exploring from that new vertice: *)
        else
          explore (Path.snoc path x) li x
      )
    in
    explore (Path.singleton s) li0 s
  in
  Symbol.fold explore_from []
  |> List.map (multicycle_of_node_list g)

let iter_on_multicycles = List.iter

(* Discard the cycles of length two with the same edge twice, as they give no
 * information. *)
let not_trivial = function
  | (_,e) :: (_,e') :: _ when e = e' -> false
  | _ -> true

let iter_on_cycles f cycles =
  (* We extract the simple cycles from a multicycle by choosing an edge in each
   * edge set. *)
  (* [David] parfait *)
  let rec explore path_suffix = function
    | []           -> if not_trivial path_suffix then f path_suffix
    | (x,set) :: q ->
        Graph.Positions.iter (fun e -> explore ((x,e)::path_suffix) q) set
  in
  let ff multicycle = explore [] (List.rev multicycle) in
  iter_on_multicycles ff cycles

(* Those functions count the number of cycles of the original graph associated
 * with a given multicycle or list of multicycles. *)
let count_cycles_for_multicycle =
  List.fold_left (fun prod (_,set) -> prod * Graph.Positions.cardinal set) 1
let count_cycles =
  List.fold_left (fun sum mc -> sum + count_cycles_for_multicycle mc) 0

(* Facilities to print cycles. *)
let print_positions file set =
  (*Graph.Positions.iter (Printf.fprintf file "%u,") set*)
  Graph.Positions.fold (fun elt sep -> Printf.fprintf file "%s%u" sep elt; ",") set ""
  |> ignore
let print_multicycle file =
  List.iter (fun (x,set) ->
    Printf.fprintf file "%c--{%a}--" (Symbol.to_char x) print_positions set
  )
let print_cycle file =
  List.iter (fun (x,e) ->
    Printf.fprintf file "%c--[%i]--" (Symbol.to_char x) e
  )

let graph_of_known_cipher clear cipher =
  let n = String.length clear in
  assert (n = String.length cipher); (* [David] bien *)
  let g = Graph.create () in
  for i = 0 to n-1 do
    Graph.add_edge g (Symbol.of_char clear.[i]) (Symbol.of_char cipher.[i]) i
  done;
  g

(** Behaviour of the "cycles" executable. *)
let () =
  if Filename.basename Sys.argv.(0) = "cycles" then begin
    match List.tl @@ Array.to_list Sys.argv with
    | clear :: cipher :: [] ->
        let clear  = String.uppercase clear
        and cipher = String.uppercase cipher in
        let g = graph_of_known_cipher clear cipher in
        let cycles = cycles g in
        let merged_count = List.length cycles in
        Printf.printf "%u merged cycles\n" merged_count;
        if merged_count <= 20 then
          iter_on_multicycles (Printf.printf "    %a\n" print_multicycle) cycles;
        let count = count_cycles cycles in
        Printf.printf "%u expanded cycles\n" count;
        (*if count <= 20 then
          iter_on_cycles (Printf.printf "    %a\n" print_cycle) cycles*)
    | _ ->
        Printf.fprintf stderr "usage:\n%s CLEAR CIPHER\n" Sys.argv.(0);
        exit 1
  end
