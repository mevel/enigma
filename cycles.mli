(** Algorithm computing the set of all (elementary) cycles of a graph. *)

(** A (multi)cycle is given as a list of tuples [(v,e)] where [v] is the visited
  * vertice and [e] is the label of the edge that starts from it. *)

(** A ‘multicycle’, whose edges are labeled by sets of integers (that is, a
  * cycle in the merged graph of a multigraph). *)
type multicycle = (Symbol.sym * Graph.Positions.t) list

(** A cycle whose edges are labeled by integers. *)
type cycle      = (Symbol.sym * int              ) list

(** The type of the value returned by [cycles] below. *)
type cycles

(** Compute the set of elementary cycles of the graph. *)
val cycles : Graph.t -> cycles

(** Iterate on a set of multicycles as returned by [cycles]. *)
val iter_on_multicycles : (multicycle -> unit) -> cycles -> unit

(** Iterate on a set of simple cycles as returned by [cycles]. *)
val iter_on_cycles      : (cycle      -> unit) -> cycles -> unit

(** Build the graph associated with a pair (clear,cipher). *)
val graph_of_known_cipher : string -> string -> Graph.t
