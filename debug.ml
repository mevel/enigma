let debug = true

let p =
  if debug then fun fmt -> Printf.eprintf fmt else fun fmt -> Printf.ifprintf stderr fmt
