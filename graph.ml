(** Implementation of non-oriented graphs with symbols as vertices
  * and integer-labelled multi-edges. *)

module Positions = Set.Make (struct type t = int let compare = (-) end)

(* Adjacency matrix seems to be a reasonable choice here for implementing the
 * graphs (non-oriented, small number of vertices, and a matrix guarantees
 * efficient accesses and mutations). *)
type t = Positions.t array array
(* [David] Symbol.Map.t aurait été préférable: cela assure que le tableau
 * fait la bonne taille et évite les conversions Symbol.to/of_int *)

(* As graphs are non-oriented, we use triangular matrix; be sure to access the
 * cell at index (i,j) only if i > j (we do not allow i = j either). *)
let create () =
  Array.init Symbol.nb_syms (fun i -> Array.make i Positions.empty)

(* This function helps ensuring that condition, as well as translating [Symbol.sym]
 * indices into integer indices. *)
let indices (x,y) =
  let i = Symbol.to_int x
  and j = Symbol.to_int y in
  assert (i <> j);
  if i > j then
    (i,j)
  else
    (j,i)

let add_edge g x y n =
  let (i,j) = indices (x,y) in
  g.(i).(j) <- Positions.add n g.(i).(j)

let get_edge g x y =
  let (i,j) = indices (x,y) in
  g.(i).(j)

let fold_over_connected g f acc0 x =
  let i = Symbol.to_int x in
  let acc = ref acc0 in
  for j = 0 to i-1 do
    if not @@ Positions.is_empty g.(i).(j) then
      acc := f !acc (Symbol.of_int j)
  done;
  for j = i+1 to Symbol.nb_syms-1 do
    if not @@ Positions.is_empty g.(j).(i) then
      acc := f !acc (Symbol.of_int j)
  done;
  !acc

(* Print the contents of a set of type [Positions.t]. *)
let print_positions file set =
  (*Positions.iter (Printf.fprintf file "%u,") set*)
  Positions.fold (fun elt sep -> Printf.fprintf file "%s%u" sep elt; ",") set ""
  |> ignore

let print_path g file = function
  | []            -> ()
  | first :: tail ->
      Printf.fprintf file "%c" (Symbol.to_char first);
      List.fold_left (fun prev cur ->
        Printf.fprintf file "--{%a}--%c" print_positions (get_edge g prev cur)
          (Symbol.to_char cur);
        cur
      ) first tail
      |> ignore
