(** Efficient representation of paths, as a utility for the computation of
  * all cycles of a graph. *)

type t = {
  rev_path : Symbol.sym list;
  source :   Symbol.sym;
  vertices : Symbol.Set.t; (* [David] parfait *)
}

let compare = Pervasives.compare

let source   {source  ;_} = source
let rev_path {rev_path;_} = rev_path
let mem      {vertices;_} x = Symbol.Set.member x vertices

let singleton x =
  { rev_path = [x]; source = x; vertices = Symbol.Set.singleton x }
let snoc path x =
  { rev_path = x :: path.rev_path;
    source   = path.source;
    vertices = Symbol.Set.add x path.vertices }
