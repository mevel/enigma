(** Efficient representation of paths, as a utility for the computation of
  * all cycles of a graph. *)

type t

val compare : t -> t -> int

val source : t -> Symbol.sym
val rev_path : t -> Symbol.sym list
val mem : t -> Symbol.sym -> bool

val singleton : Symbol.sym -> t
val snoc : t -> Symbol.sym -> t
