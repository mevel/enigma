(** Representation and manipulation of characters A-Z, as well as
  * functions on such characters and subsets of those characters. *)

type sym = int

let a = 0

let nb_syms = 26

(* [David] of_*: invariants non assurés *)

let of_char c = Char.code c - Char.code 'A'
let to_char x = Char.chr (x + Char.code 'A')

let of_int n = n
let to_int x = x

let next x = (x+1) mod nb_syms

let (++) x y = (x+y) mod nb_syms
let (--) x y = (x-y+nb_syms) mod nb_syms

let iter f =
  for x = 0 to nb_syms-1 do
    f x
  done

let fold f =
  let rec fold x acc =
    if x = nb_syms then
      acc
    else
      fold (x+1) (f acc x)
  in
  fold 0

module Set = struct
  (* By using int as bitmasks we are limited to Sys.word_size-1 bits. *)
  assert (nb_syms < Sys.word_size) (* [David] bien *)
  type t = int
  let empty = 0
  let singleton = (lsl) 1
  let member x set = (set lsr x) land 1 <> 0
  let add    x set =  set lor singleton x
end

module Map = struct
  (* [David] parfait *)
  type 'a t = 'a array
  let get = Array.get
  let set = Array.set
  let make y = Array.make nb_syms y
  let init f = Array.init nb_syms f
  let copy = Array.copy
  let map = Array.map
  let inverse m =
    let inv = make 0 in
    iter (fun x ->
      inv.(m.(x)) <- x
    );
    inv
  let print_tmap file =
    Array.iter (fun y ->
      output_char file (to_char y)
    )
end
